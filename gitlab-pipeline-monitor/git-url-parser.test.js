const sut = require('./git-url-parser');

describe(`The following git remote urls should be supported`, () => {
  [
    {
      url: 'git@gitlab.com:balazs4/gitlab-pipeline-monitor.git',
      expected: {
        domain: 'gitlab.com',
        project: 'balazs4/gitlab-pipeline-monitor'
      }
    },
    {
      url: 'https://gitlab.com/balazs4/gitlab-pipeline-monitor.git',
      expected: {
        domain: 'gitlab.com',
        project: 'balazs4/gitlab-pipeline-monitor'
      }
    },
    {
      // https://gitlab.com/balazs4/gitlab-pipeline-monitor/issues/12
      url: 'ssh://git@gitlab.gf.com.cn:10022/gf-hybrid/rn-service.git',
      expected: {
        domain: 'gitlab.gf.com.cn',
        project: 'gf-hybrid/rn-service'
      }
    },
    {
      url: 'rsync://host.xz/path/to/repo.git',
      expected: {
        domain: 'host.xz',
        project: 'path/to/repo'
      }
    },
    {
      url: 'host.xz:~user/path/to/repo.git',
      expected: {
        domain: 'host.xz',
        project: 'user/path/to/repo'
      }
    },
    {
      url: 'user@host.xz:/path/to/repo.git',
      expected: {
        domain: 'host.xz',
        project: 'path/to/repo'
      }
    },
    {
      url: 'rsync://host.xz/path/to/repo.git',
      expected: {
        domain: 'host.xz',
        project: 'path/to/repo'
      }
    }
  ].forEach(x => {
    it(`${x.url}`, () => expect(sut(x.url)).toEqual(x.expected));
  });
});
