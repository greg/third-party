const vscode = require('vscode');

const refresh = () => {
  vscode.gitLabWorkflow.sidebarDataProviders.forEach(provider => {
    provider.refresh();
  });
};

const sortAssignedIssues = () => {
  vscode.gitLabWorkflow.sidebarDataProviders.forEach(provider => {
    if (provider.fetcher !== 'fetchIssuesAssignedToMe') {
      return;
    }
    provider.sort();
  });
};

const sortCreatedIssues = () => {
  vscode.gitLabWorkflow.sidebarDataProviders.forEach(provider => {
    if (provider.fetcher !== 'fetchIssuesCreatedByMe') {
      return;
    }
    provider.sort();
  });
};

const sortAllIssues = () => {
  vscode.gitLabWorkflow.sidebarDataProviders.forEach(provider => {
    if (provider.fetcher !== 'fetchAllProjectIssues') {
      return;
    }
    provider.sort();
  });
};

const sortAssignedMergeRequests = () => {
  vscode.gitLabWorkflow.sidebarDataProviders.forEach(provider => {
    if (provider.fetcher !== 'fetchMergeRequestsAssignedToMe') {
      return;
    }
    provider.sort();
  });
};

const sortCreatedMergeRequests = () => {
  vscode.gitLabWorkflow.sidebarDataProviders.forEach(provider => {
    if (provider.fetcher !== 'fetchMergeRequestsCreatedByMe') {
      return;
    }
    provider.sort();
  });
};

const sortAllMergeRequests = () => {
  vscode.gitLabWorkflow.sidebarDataProviders.forEach(provider => {
    if (provider.fetcher !== 'fetchAllProjectMergeRequests') {
      return;
    }
    provider.sort();
  });
};

exports.refresh = refresh;
exports.sortAssignedIssues = sortAssignedIssues;
exports.sortCreatedIssues = sortCreatedIssues;
exports.sortAllIssues = sortAllIssues;
exports.sortAssignedMergeRequests = sortAssignedMergeRequests;
exports.sortCreatedMergeRequests = sortCreatedMergeRequests;
exports.sortAllMergeRequests = sortAllMergeRequests;
